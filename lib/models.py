from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

db = SQLAlchemy()


class SentimentStat(db.Model):
    __tablename__ = 'sentiment_stats'

    time = db.Column(db.DateTime, primary_key=True)
    tweets_processed = db.Column(db.Integer, nullable=False)
    sentiment_score = db.Column(db.Float, nullable=False)

    def __repr__(self):
        formatted_time = self.time.isoformat()
        return "{" + str(self.tweets_processed) + " with score " + \
               str(self.sentiment_score) + " at time " + formatted_time + "}"


class SavedTweet(db.Model):
    __tablename__ = 'saved_tweets'

    tweet_id = db.Column(db.String(100), primary_key=True)
    time = db.Column(db.DateTime, nullable=False)
    text = db.Column(db.Text, nullable=False)
    sentiment_score = db.Column(db.Float, nullable=False)

    def __repr__(self):
        formatted_time = self.time.isoformat()
        return "{" + self.text + "; with score " + \
               self.sentiment_score + " at time " + formatted_time + "}"


class User(UserMixin, db.Model):
    """An admin user capable of viewing sentiment stats.

    :param str email: email address of user
    :param str password: encrypted password for the user

    """
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    password = db.Column(db.String(200), nullable=False)
    created_on = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=True
    )
    last_login = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=True
    )

    def set_password(self, password):
        """Create hashed password."""
        self.password = generate_password_hash(
            password,
            method='sha256'
        )

    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

