import yaml


class Config:
    def __init__(self, file_path):
        with open(file_path) as file:
            self.raw_data = yaml.load(file, Loader=yaml.SafeLoader)

    def twitter_credentials(self):
        return self.raw_data['twitter']

    def google_credentials(self):
        return self.raw_data['google']

    def csrf_key(self):
        return self.raw_data['csrf']['secret_key']

    def get(self, key):
        return self.raw_data[key]
