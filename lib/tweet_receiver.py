from textblob import TextBlob
import tweepy

from lib.models import db
from lib.models import SavedTweet


class TweetEventListener(tweepy.StreamListener):

    def on_status(self, tweet):
        meaningful_text = tweet.text
        if tweet.entities.get('urls'):
            for url in tweet.entities['urls']:
                if url.get('unwound'):
                    meaningful_text += '\n' + url['unwound']['title']
        blob = TextBlob(meaningful_text)

        saved_tweet = SavedTweet()
        saved_tweet.tweet_id = tweet.id_str
        saved_tweet.text = meaningful_text
        saved_tweet.sentiment_score = blob.sentiment.polarity
        saved_tweet.time = tweet.created_at
        db.session.add(saved_tweet)
        db.session.commit()

    def on_error(self, status_code):
        if status_code == 420:
            print('Rate exceeded')
            # returning False in on_error disconnects the stream
            return False


class TweetReceiver:

    def __init__(self, consumer_key, consumer_secret, access_token, access_secret):
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_secret)
        self.api = tweepy.API(auth)

    def get_tweets(self, conditions):
        tweet_listener = TweetEventListener()
        tweets = tweepy.Stream(auth=self.api.auth, listener=tweet_listener)
        tweets.filter(**conditions)
