from google.cloud import logging

from flask import Blueprint
from flask import current_app
from flask import Flask
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask.cli import AppGroup
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_login import login_user
from flask_login import login_required
from flask_login import current_user
from flask_login import logout_user
from sqlalchemy import and_
from sqlalchemy import func
from statistics import mean

import chartkick
import click
import datetime
import gevent
import os
import psycogreen.gevent
import threading

from lib.config import Config
from lib.forms import LoginForm
from lib.models import db
from lib.models import SavedTweet
from lib.models import SentimentStat
from lib.models import User
from lib.tweet_receiver import TweetReceiver


psycogreen.gevent.patch_psycopg()

login_manager = LoginManager()
custom_config = Config('config.yaml')

ck = Blueprint('ck_page', __name__, static_folder=chartkick.js(), static_url_path='/static')
main = Blueprint('main', __name__)
command_line = AppGroup('command')

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = custom_config.google_credentials()['api_credential_path']

logging_client = logging.Client()
log_name = custom_config.get('request_log_name')  # log files may vary between development and production
logger = logging_client.logger(log_name)

def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = custom_config.csrf_key()
    app.config['SQLALCHEMY_DATABASE_URI'] = custom_config.get('db_uri')
    db.init_app(app)
    with app.app_context():
        db.create_all()

    app.register_blueprint(ck, url_prefix='/ck')
    app.register_blueprint(main)
    app.cli.add_command(command_line)

    app.jinja_env.add_extension("chartkick.ext.charts")

    bootstrap = Bootstrap(app)

    login_manager.init_app(app)
    twitter_stream_thread = threading.Thread(target=fetch_tweets_in_background,
                                             kwargs={'app': app},
                                             name="Tweet Downloader")
    twitter_stream_thread.start()

    return app


def fetch_tweets_in_background(app):
    twitter_credentials = custom_config.twitter_credentials()
    tweet_source = TweetReceiver(twitter_credentials['api_key'],
                                 twitter_credentials['api_secret'],
                                 twitter_credentials['access_token'],
                                 twitter_credentials['access_secret'])

    with app.app_context():
        tweet_source.get_tweets({'track': ['fast fashion', 'fast-fashion'],
                                 'languages': ['en']})


@main.route('/')
def index_page():
    logger.log_struct({
        'ip': request.remote_addr,
        'path': '/',
        'params': request.values
    })
    current_stat \
        = db.session.query(SentimentStat).order_by(SentimentStat.time.desc()).first()

    today = datetime.datetime.today().strftime("%Y-%m-%d")
    date_from = request.args.get('date_from')
    if date_from is None or date_from > today:
        date_from = today
    date_to = request.args.get('date_to')
    if date_to is None or date_to > today:
        date_to = today
    if date_from > date_to:
        date_from, date_to = date_to, date_from
    try:
        historic_data = \
            db.session.query(SentimentStat).filter(
                and_(func.date(SentimentStat.time) >= date_from,
                     func.date(SentimentStat.time) <= date_to)
            ).order_by(SentimentStat.time.desc()).all()
        data_for_score_chart = [[stat.time.isoformat(), stat.sentiment_score]
                                for stat in historic_data]
        data_for_tweets_chart = [[stat.time.isoformat(), stat.tweets_processed]
                                 for stat in historic_data]
    except Exception as err:  # something went wrong while fetching from the DB
        logger.error(err)
        raise err  # re-raising to signal the crash to Gunicorn

    if len(historic_data) > 0:
        avg_score = mean([stat.sentiment_score for stat in historic_data])
        avg_tweets = mean([stat.tweets_processed for stat in historic_data])
    else:
        avg_score, avg_tweets = 'unknown', 'unknown'

    data_for_charts = {
        'all_scores': data_for_score_chart,
        'avg_score': avg_score,
        'all_tweets': data_for_tweets_chart,
        'avg_tweets': avg_tweets
    }
    return render_template('index.html',
                           current_stat=current_stat,
                           historic_data=data_for_charts,
                           date_from=date_from,
                           date_to=date_to)


@main.route('/login', methods=['GET', 'POST'])
def login():
    """
    Log-in page for registered users.

    GET requests serve Log-in page.
    POST requests validate and redirect user to dashboard.
    """
    logger.log_struct({
        'ip': request.remote_addr,
        'method': request.method,
        'path': '/',
        'params': request.values,
        'cookie': request.cookies,
    })

    # Bypass if user is logged in
    if current_user.is_authenticated:
        return redirect(url_for('main.index_page'))

    form = LoginForm()
    # Validate login attempt
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(email=form.email.data).first()
            if user and user.check_password(password=form.password.data):
                login_user(user)
                next_page = request.args.get('next')
                logger.log_struct({
                    'ip': request.remote_addr,
                    'user': user.email,
                    'message': 'Login successful'
                })
                return redirect(next_page or url_for('main.index_page'))
        except Exception as err:  # something went wrong while fetching the user data
            logger.error(err)
            raise err  # re-raising to signal the crash to gUnicorn

        flash('Invalid username/password combination')
        logger.log_struct({
            'ip': request.remote_addr,
            'user': form.email.data,
            'message': 'Login failed'
        })
        return redirect(url_for('main.login'))
    return render_template(
        'login.html',
        form=form,
        title='Log in.',
        template='login-page',
        body="Log in with your User account."
    )


@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return User.query.get(user_id)
    return None


@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash('You must be logged in to view that page.')
    return redirect(url_for('main.login'))


@main.route("/logout", methods=["GET"])
@login_required
def logout():
    """Logout the current user."""
    logout_user()
    return redirect(url_for('main.index_page'))


@command_line.command('create_user')
@click.argument('email')
@click.argument('name')
def create_user(email, name):
    new_user = User()
    new_user.email = email
    new_user.name = name
    password = click.prompt('Input password', hide_input=True)
    new_user.set_password(password)
    db.session.add(new_user)
    db.session.commit()
    print('User created')
    os._exit(0)


@command_line.command('aggregate_tweets')
def aggregate_tweets():
    tweets = db.session.query(SavedTweet).all()
    sum_sentiments = sum([tweet.sentiment_score for tweet in tweets])
    num_tweets = len(tweets)

    if num_tweets > 0:
        current_stat = SentimentStat()
        current_stat.time = datetime.datetime.now()
        current_stat.sentiment_score = sum_sentiments / num_tweets
        current_stat.tweets_processed = num_tweets
        db.session.add(current_stat)
        db.session.query(SavedTweet).delete()
        db.session.commit()

    print(str(num_tweets) + " tweets aggregated")
    # the only way I could make it exit properly, as Flask catches all exceptions
    os._exit(0)


if __name__ == '__main__':
    current_app.run()
